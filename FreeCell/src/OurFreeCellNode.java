import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

public class OurFreeCellNode extends SearchNode{
	//Works.
	class Node{
		//public Node next = null;
		public Node prev = null;
		public Card card;
		public Node(Card card, Node prev) {
			this.card = card;
			this.prev = prev;
		}
		public Node(Node node) {
			this.prev = null;
			this.card = node.card;
		}
	}
	
	//Node is the accessible tail.
	Node[] cascades = new Node[8]; //8 arrays of Nodes
	//Node[] cascadeTails = new Node[8]; //stores the face up node for quick access
	int[] foundRanks = new int[4]; //CHSD
	Card[] freeCells = new Card[4];
	Card prevMove = null;

	//Works.
	public OurFreeCellNode (int seed) {	
		Stack<Card> cards = Card.getShuffle(seed);
		int size = 0;
		// initialize the cascades
		for (int i = 0; i < 8; i++) {
			int maxCards = 8;
			if(i >= 4) {
				maxCards = 7;
			}
			else {//initialize foundRanks
				foundRanks[i] = -1;
			}
			Node ptr = null;
//			size++;
			for (int j = 0; j < maxCards-1; j++) {
				Node node = new Node(cards.pop(), ptr);
				ptr = node;
			}
//			Node node = new Node(cards.pop(), ptr);
			cascades[i] = ptr;
		}
	}

	//Works.
	public String toString() {
		String str = "Foundation:";
		for(int i = 0; i < 4; i++) {
			if(foundRanks[i] == -1){
				str += " null";
			}
			else {
				str += " " + foundRanks[i];
			}
		}
		str += "  FreeCells:";
		for(Card card : freeCells) {
			str += " " + card;
		}
		str += "\n";
		str += "Cascades: \n";
		String[][] cascs = new String[8][12];
		for (int i = 0; i < 8; i++) {
			int j = 0;
			Node ptr = cascades[i];
			while (ptr != null) {
				cascs[i][j] = "" + ptr.card;
				ptr = ptr.prev;
				j++;
			}
		}
		
		for(int i = 0; i < 12; i++) {
			for (int j = 0; j < 8; j++) {
				if(i == 0) {
					str += " " + j + " ";
				}
				else if(cascs[j][i-1] != null) {
					str += " " + cascs[j][i-1];
				}
				else {
					str += "  ";
				}
			}
			str += "\n";
			
		}
		return str;
	}

	//Works.
	@Override
	public boolean isGoal() {
		//All foundations have Kings, all cascades and freeCells are empty
		for(int i = 0; i < 8; i++) {
			if((i < 4) && (foundRanks[i] != 12 || freeCells[i] != null)) {
				return false;
			}
			else if (cascades[i] != null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check auto moves to foundation
	 */
	private void safeMove() {
		for(int i = 0; i < 4; i++) {
			//FreeCell to Foundation
			if(freeCells[i] != null) {
				Card from = isLegal(freeCells[i], i); 
				if (from != null) {
					foundRanks[i] = from.rank;
					freeCells[i] = null;
				}	
			}
			for(int j = 0; j < 8; j++) {
				//Cascade to Foundation
				Card from = isLegal(cascades[j], i); 
//				System.out.println("auto? " + from);
				while (from != null) {
					foundRanks[i] = from.rank; //add to foundation
					Node fromNode = cascades[j]; //store the node
					cascades[j] = cascades[j].prev; //replace the card in cascade
					fromNode.prev = null; //remove node from memory (let it be collected).
					from = isLegal(cascades[j], foundRanks[i]); //Next iteration.
				}
			}
		}
	}
	
	@Override
	public Object clone() {
		OurFreeCellNode clone = (OurFreeCellNode) super.clone();
		clone.cascades = new Node[8]; //8 arrays of Nodes
		clone.foundRanks = new int[4];
		clone.freeCells = new Card[4];

		for(int i = 0; i < 8; i++) { //Clone Cascades
			if(cascades[i] != null) {
				clone.cascades[i] = new Node(cascades[i]); 
				Node parent = cascades[i].prev;
				Node ptr = clone.cascades[i];
				while( parent != null) { 
					ptr.prev = new Node(parent);
					parent = parent.prev;
					ptr = ptr.prev;
				}
			}
		}
		for(int i = 0; i < 4; i++) { //CLone Freecells and Foundranks
			clone.foundRanks[i] = foundRanks[i];
			clone.freeCells[i] = freeCells[i];
		}
		return clone;
	}

	public int boardEval() {
		int result = 0;
		//Number of cards in home - most important
		for(int i = 0; i < 4; i++) {
			int c = foundRanks[i];
			if(c != -1) {
				result += foundRanks[i]*2;
			}
		}
		
		//Number of open freeCells
		for(int i = 0; i < 4; i++) {
			if(freeCells[i] == null) {
				result++;
			}
		}
		
		//Number of empty cascades.
		for(int i = 0; i < 8; i++) {
			if(cascades[i].card == null) {
				result++;
			}
		}
		return result;
	}
	
	@Override
	public ArrayList<SearchNode> expand() {
		ArrayList<SearchNode> children = new ArrayList<SearchNode>();
		safeMove(); //Make auto plays
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				Card from;
				if(i < 4) {
					//Free Cell to Cascades
					if(freeCells[i] != null) {
						from = isLegal(freeCells[i], cascades[j]);
						if (from != null && from != prevMove) {
							OurFreeCellNode child = (OurFreeCellNode) this.clone();
							Node cFrom = new Node(child.freeCells[i], cascades[j]);
							child.freeCells[i] = null;
							child.prevMove = from;
							children.add(child);
							from = null;
						}
					}
					//Cascade to FreeCell 
					else if (freeCells[i] == null && cascades[j] != null && cascades[j].card != prevMove) { //You can always move 1 card to a freeCell if free
						from = cascades[j].card;
						OurFreeCellNode child = (OurFreeCellNode) this.clone();
						child.freeCells[i] = from;
						Node fromNode = child.cascades[j];
						child.cascades[j] = child.cascades[j].prev;
						fromNode.prev = null;
						child.prevMove = from;
						children.add(child);
						from = null;
					}
				}
				else {//Cascade to Cascade
					if(cascades[i] != null) {
						Node fromNode = isLegal(cascades[i], cascades[j]); 
						if (fromNode != null && fromNode.card != prevMove) {
							OurFreeCellNode child = (OurFreeCellNode) this.clone();
							if(fromNode == cascades[i]) { //Not supermove
								Node childFromNode = child.cascades[i];//store the from Node
								child.cascades[i] = fromNode.prev; //child will be the Parent-F-Node's prev. node.  
								childFromNode.prev = cascades[j]; //child-F-Node's prev is the parent's to node
								child.prevMove = childFromNode.card;
							}
							else { //supermove - different fromNode.
								Node childFromNode = child.cascades[i];
								while(fromNode.card != childFromNode.card) {
									childFromNode = childFromNode.prev;
								}
								child.cascades[i] = fromNode.prev; //nullify the nodes that were moved.  
								childFromNode.prev = cascades[j]; //child-F-Node's prev is the parent's to node
								child.prevMove = childFromNode.card;
							}
							children.add(child);
							from = null;
						}
					}
				}
			}
		}
		return children;
	}

	/**
	 * Cascade to Cascade
	 * @param node
	 * @param node2
	 * @return
	 */
	private Node isLegal(Node node, Node node2) {
		Node fromNode = node; //card that will be moved
		//Check for supermoves
		if(node.prev != null) {
			Node prev = node.prev; //card above it (last card that stays).
			//			System.out.println("prev card rank: " + prev.card.rank);
			while(prev != null && fromNode.card.rank + 1 == prev.card.rank &&
					fromNode.card.isRed() != prev.card.isRed()) {
				fromNode = prev;
				prev = prev.prev;
			}
		}
		
		if(node2 == null) {//can always move to a empty cascade
			return fromNode;
		}
		
		Card from = fromNode.card;
		Card to = node2.card;
		//Check that the card belongs on the pile.
		if((from.rank != to.rank - 1) || (from.isRed() == to.isRed())) {
			return null;
		}
		return fromNode;
	}

	/**
	 * Cascades to Foundations
	 * @param node
	 * @param i		Index of foundRanks of the card. (CHSD)
	 * @return
	 */
	private Card isLegal(Node node, int i) {
		if(node == null) {
			return null;
		}
		Card from = node.card;
		//Check for the same suits & Check that the card belongs on the pile.
		if(from.suit != i || from.rank != foundRanks[i]+1) {
//			System.out.println("i: " + i);
//			System.out.println("Suit: " + from.suit + " next rank: " + foundRanks[i]+1 
//					+ " i: " +i +  " rank: " + from.rank);
			return null;
		}
		return from;
	}


	/**
	 * FreeCell to Cascade
	 * @param card
	 * @param node
	 * @return
	 */
	private Card isLegal(Card from, Node node) {
		//If the node is empty.
		if(node != null) {
			Card to = node.card;
			//Check that the card belongs on the pile.
			if((from.rank != to.rank - 1) || (from.isRed() == to.isRed())) {
				return null;
			}
		}

		return from;
	}

	/**
	 * FreeCell to Foundations
	 * @param card
	 * @param i		Index of foundRanks of the card. (CHSD)
	 * @return
	 */
	private Card isLegal(Card from, int i) {
		//Check for the same suits & Check that the card belongs on the pile.
		if(from.suit != i || from.rank != foundRanks[i]+1) {
			return null;
		}
		return from;
	}

	public static void main(String[] args) {
//		OurFreeCellNode board = new OurFreeCellNode(1);
//		System.out.println("Parent: " + board);
//		ArrayList<SearchNode> children = board.expand();
//		for(SearchNode sn : children) {
//			System.out.println("Child " + sn);
//		}
		
		
//		Random random = new Random();
//		FreeCellNode board = new FreeCellNode(random.nextInt(1000000) + 1);
//		System.out.println(board);
//		for(int i = 0; i < 8; i++) {
//			if(i < 4) {
//				board.foundRanks[i] = 12;
//				board.freeCells[i] = null;
//			}
//			board.cascades[i] = null;
//		}
//
//		System.out.println(board);
//		System.out.println("win? " + board.isGoal());
//
//		FreeCellNode board1 = new FreeCellNode(random.nextInt(1000000) + 1);
//		System.out.println(board1);
//
//		System.out.println("CLONE:");
//		FreeCellNode board2 = (FreeCellNode) board1.clone();
//		System.out.println(board2); //child
//
//		System.out.println("Modify CHild (deleing row 2):");
//
//		board2.cascades[2] = null; //modify child
//
//		System.out.println("CLONE:");
//		System.out.println(board2);
//
//		System.out.println("Parent:");
//		System.out.println(board1);
//		
//		Scanner in = new Scanner(System.in);
//		System.out.println("Eval: " + board1.boardEval());
//		System.out.println("From: 0 = cascade, 1 = freeCell");
//		while (true){
//			
//			int fromS = in.nextInt();
//			if(fromS == 0) {
//				System.out.println("casc #");
//				int from = in.nextInt();
//				System.out.println("to casc(0) or (1)fre?");
//				int toS = in.nextInt();
//				if(toS == 0) {
//					System.out.println("casc #");
//					int to = in.nextInt();
//					System.out.println("card from " + board1.cascades[from].card + 
//							" card to: " + board1.cascades[to].card);
//					System.out.println("Move Legal: " 
//							+ board1.isLegal(board1.cascades[from], board1.cascades[to]).card);
//				}
//				else {
//					System.out.println("freeCell #");
//					int to = in.nextInt();
//					if (board1.freeCells[to] == null && board1.cascades[from] != null) {
//						System.out.println("Move Legal: true " + board1.cascades[from].card);
//					}
//				}
//			}
//			else {
//				System.out.println("freecell #");
//				int from = in.nextInt();
//				System.out.println("casc #");
//				int to = in.nextInt();
//				System.out.println("Move Legal: " 
//						+ board1.isLegal(board1.freeCells[from], 
//								board1.cascades[to]));
//			}
//			System.out.println(board1);
//			System.out.println("Eval: " + board1.boardEval());
//			System.out.println("From: casc = cascade, fre = freeCell");
//		}
		

		/* Searcher to see if we get a solution.*/
		// Create the searcher object (uncomment desired code)

		Searcher searcher = new HeuristicSearcher(5);
//		Searcher searcher = new BreadthFirstSearcher();
//		Searcher searcher = new RecursiveDepthLimitedSearcher(7);
		//Searcher searcher = new DepthFirstSearcher();
		//Searcher searcher = new RecursiveDepthFirstSearcher();
//		Searcher searcher = new DepthLimitedSearcher(7);
		//Searcher searcher = new IterativeDeepeningDepthFirstSearcher();

		// Create the root search node (uncomment desired code)

		SearchNode root = new FreeCellNode(3);
		//SearchNode root = new PegSolitaireNode();
		//SearchNode root = new BucketsNode();
		//LightsOutNode root = new LightsOutNode(4); root.makeRandomMoves(1000);
		//TilePuzzleNode root = new TilePuzzleNode(4); root.makeRandomMoves(8);

		if (searcher.search(root)) {
			// successful search
			System.out.println("Goal node found in " + searcher.getNodeCount() 
			+ " nodes.");
			System.out.println("Goal path:");
			searcher.printGoalPath();
		} else {
			// unsuccessful search
			System.out.println("Goal node not found in " 
					+ searcher.getNodeCount() + " nodes.");
		}

	}


}
