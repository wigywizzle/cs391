import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class HeuristicSearcher extends Searcher{
	int depthLimit;
	//long nodeCount;
	long start;
	/**
	 * @param depthLimit  The number of children to look
	 * ahead for the best possible child node.
	 */
	public HeuristicSearcher(int depthLimit) {
		this.depthLimit = depthLimit;
		this.start = System.currentTimeMillis();
	}

	@Override
	public boolean search(SearchNode node) {
		return search(node, depthLimit);
	}

	/**
	 * Will give the future best outcome of the child.
	 * @param node
	 * @param depthLimit
	 * @return
	 */
	public SearchNode hSearch (SearchNode node, int depthLimit) {
		nodeCount++;
		if (node.isGoal()) {
			goalNode = node;
			return node;
		}
		ArrayList<SearchNode> children = node.expand(); 
		if(depthLimit > 0 && !children.isEmpty()) { 
			FreeCellNode bestNode = null;
			for(SearchNode child : children) {
				FreeCellNode childNode = (FreeCellNode) hSearch(child, depthLimit-1);
				if(childNode == null ) {
					continue;
				}
				else if(childNode.isGoal()) {
					return childNode;
				}
				else if( (bestNode == null
						|| childNode.boardEval() > bestNode.boardEval())){
					bestNode = childNode;
				}
			}
			return bestNode;
		}
		return node;
	}

	public boolean search(SearchNode node, int depthLimit) {

		if (node == null){
			System.out.println("Initial node null");
		}

		Stack<ArrayList<SearchNode>> prevChildren = new Stack<ArrayList<SearchNode>>();
		
		HashMap<Integer,SearchNode> prevStates = new HashMap<Integer,SearchNode>();
		
		while (node != null && !node.isGoal() && System.currentTimeMillis() - start < 60000){
			ArrayList<SearchNode> children = node.expand(); 
			FreeCellNode bestNode = null;
			if ((!prevChildren.isEmpty() && prevStates.containsKey(node.toString().hashCode())) 
					|| (children.isEmpty() && !node.isGoal())){		
				//System.out.println("First if statement");
				for (int i = depthLimit; !prevChildren.isEmpty() && children.isEmpty() && !node.isGoal() && i < 8 ; i++){
					children = prevChildren.pop();
					//System.out.println("Depth: " + i);
					for (SearchNode child : children) { 
						FreeCellNode childNode = (FreeCellNode) hSearch(child, i);
						if(!prevStates.containsKey(childNode.toString().hashCode()) && childNode != null && (bestNode == null 
								|| childNode.boardEval() > bestNode.boardEval()) ){ //&&
//							(childNode.expand().size() != 1 || childNode.expand().get(0).isGoal())
							
							bestNode = childNode;
						}
						
					}
					if(bestNode != null && bestNode.isGoal()){
						goalNode = node;
						return true;
					}
					if(bestNode != null) {
						children = bestNode.expand();
					}
					
				}
				if(children.isEmpty()) {
					System.out.println("Couldnt find a path");
					return false;
				}
			}
			else{
				prevChildren.add(children);
				//System.out.println("Else statement");

				for (SearchNode child : children) { 
					FreeCellNode childNode = (FreeCellNode) hSearch(child, depthLimit);
					if(childNode != null) {
						if(childNode.isGoal()) {
							goalNode = node;
							return true;
						}
						else if(!prevStates.containsKey(childNode.toString().hashCode()) || (bestNode == null 
								|| childNode.boardEval() > bestNode.boardEval()) ){ //&& (childNode.expand().size() != 1
							//|| childNode.expand().get(0).isGoal())
							
							bestNode = childNode;
						}
					}

				}
			}
			if(bestNode != null) {
				prevStates.put(node.toString().hashCode(), node);
				node = bestNode;
				//System.out.println("NODE" + node);
			}
		}
		if(node == null) {
			System.out.println("Incorrect path chosen.");
			goalNode = node;
			printGoalPath();
			return false;
		}
		
		if (node.isGoal()) {
			goalNode = node;
			return true;
		}

		System.out.println("Ran over 60 seconds: " + (System.currentTimeMillis() - start)/1000 + "seconds");
		return false;
	}

}