import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class FreeCellNode extends SearchNode {
	static final int NONE = -1, NUM_CELLS = 4, NUM_CASCADES = 8;
	int[] homeRank;
	int numCellsFree;
	Card[] cell;
	int[] cascadeSize;
	Node[] cascade;
	String prevMove = "(none)";

	class Node {
		Card card;
		Node next;

		/**
		 * @param card card card
		 * @param next next card down in cascade
		 */
		public Node(Card card, Node next) {
			this.card = card;
			this.next = next;
		}

		/**
		 * Copy constructor
		 * @param node
		 */
		public Node(Node node) {
			card = node.card;
			next = node.next;
		}
	}

	public FreeCellNode(int seed) {
		Stack<Card> deck = Card.getShuffle(seed);
		homeRank = new int[Card.NUM_SUITS];
		Arrays.fill(homeRank, NONE);
		numCellsFree = NUM_CELLS;
		cell = new Card[NUM_CELLS];
		cascadeSize = new int[NUM_CASCADES];
		cascade = new Node[NUM_CASCADES];
		for (int i = 0; i < Card.NUM_CARDS; i++) {
			Node newNode = new Node(deck.pop(), cascade[i % NUM_CASCADES]);
			cascade[i % NUM_CASCADES] = newNode;
			cascadeSize[i % NUM_CASCADES]++;
		}
		safeAutoPlay();
	}

	void safeAutoPlay() {
		boolean played = true;
		while (played) {
			played = false;

			// Cells
			for (int i = 0; i < NUM_CELLS; i++) {
				Card c = cell[i];
				if (c == null)
					continue;
				int rank = homeRank[c.suit];
				if (rank == NONE) {
					if (c.rank == 0) { // Clearing Ace is always safe
						played = true;
						homeRank[c.suit] = c.rank;
						cell[i] = null;
						numCellsFree++;
					}
				}
				// Note: legal NetCELL safe autoplay code here depends on 4 alternating red/black suits in Card.
				else if (c.rank == homeRank[c.suit] + 1 // successive home rank
						&& c.rank - 2 <= homeRank[(c.suit + 1) % 4] // opposite color home rank within 2 ranks
								&& c.rank - 3 <= homeRank[(c.suit + 2) % 4] // other same color home rank within 3 ranks
										&& c.rank - 2 <= homeRank[(c.suit + 3) % 4]) { // other opposite color home rank within 2 ranks
					played = true;
					homeRank[c.suit] = c.rank;
					cell[i] = null;
					numCellsFree++;
				}
			}

			// Cascades
			for (int i = 0; i < NUM_CASCADES; i++) {
				if (cascade[i] == null)
					continue;
				Card c = cascade[i].card;
				int rank = homeRank[c.suit];
				if (rank == NONE) {
					if (c.rank == 0) { // Clearing Ace is always safe
						played = true;
						homeRank[c.suit] = c.rank;
						cascade[i] = cascade[i].next;
						cascadeSize[i]--;
					}
				}
				// Note: legal NetCELL safe autoplay code here depends on 4 alternating red/black suits in Card.
				else if (c.rank == homeRank[c.suit] + 1 // successive home rank
						&& c.rank - 2 <= homeRank[(c.suit + 1) % 4] // opposite color home rank within 2 ranks
								&& c.rank - 3 <= homeRank[(c.suit + 2) % 4] // other same color home rank within 3 ranks
										&& c.rank - 2 <= homeRank[(c.suit + 3) % 4]) { // other opposite color home rank within 2 ranks
					played = true;
					homeRank[c.suit] = c.rank;
					cascade[i] = cascade[i].next;
					cascadeSize[i]--;
				}
			}
		}
	}


	@Override
	public boolean isGoal() {
		for (int i = 0; i < Card.NUM_SUITS; i++)
			if (homeRank[i] != Card.NUM_RANKS - 1)
				return false;
		return true;
	}

	@Override
	public ArrayList<SearchNode> expand() {
		ArrayList<SearchNode> children = new ArrayList<SearchNode>();

		// Add children to the list with high priority moves first.

		// Moves from cells to home (possibly not safe)
		for (int i = 0; i < NUM_CELLS; i++) {
			Card c = cell[i];

			if (c == null)
				continue;

			String move = c.toString();
			if (c.rank == homeRank[c.suit] + 1 && !prevMove.equals(move)) {
				FreeCellNode child = (FreeCellNode) childClone();
				child.homeRank[c.suit] = c.rank;
				child.cell[i] = null;
				child.numCellsFree++;
				child.prevMove = move;
				children.add(child);
			}
		}

		// Moves from cascades to home (possibly not safe)
		for (int i = 0; i < NUM_CASCADES; i++) {
			Node n = cascade[i];
			if (n == null)
				continue;
			Card c = n.card;
			String move = c.toString();
			if (c.rank == homeRank[c.suit] + 1 && !prevMove.equals(move)) {
				FreeCellNode child = (FreeCellNode) childClone();
				child.homeRank[c.suit] = c.rank;
				child.cascade[i] = n.next;
				child.cascadeSize[i]--;
				child.prevMove = move;
				children.add(child);
			}
		}

		// Moves from cells to non-empty cascades
		int numEmptyCascades = 0;
		for (int i = 0; i < NUM_CASCADES; i++)
			if (cascade[i] != null) {
				Card top = cascade[i].card;
				for (int j = 0; j < NUM_CELLS; j++)
					if (cell[j] != null) {
						String move = cell[j].toString();
						if (cell[j].rank + 1 == top.rank && cell[j].isRed() != top.isRed() && !move.equals(prevMove)) {
							FreeCellNode child = (FreeCellNode) childClone();
							child.cascade[i] = new Node(child.cell[j], child.cascade[i]);
							child.cascadeSize[i]++;
							child.cell[j] = null;
							child.numCellsFree++;
							child.prevMove = move;
							children.add(child);
						}
					}
			}
			else
				numEmptyCascades++;


		// Moves from cells to empty cascade
		int leftmostEmptyCascade = NONE;
		for (int i = 0; i < NUM_CASCADES; i++)
			if (cascade[i] == null) {
				leftmostEmptyCascade = i;
				break;
			}		
		if (leftmostEmptyCascade != NONE) {
			int i = leftmostEmptyCascade;
			for (int j = 0; j < NUM_CELLS; j++)
				if (cell[j] != null) {
					String move = cell[j].toString();
					if (!move.equals(prevMove)){
						FreeCellNode child = (FreeCellNode) childClone();
						child.cascade[i] = new Node(child.cell[j], child.cascade[i]);
						child.cascadeSize[i]++;
						child.cell[j] = null;
						child.numCellsFree++;
						child.prevMove = move;
						children.add(child);
					}
				}
		}

		// Moves from cascades to non-empty cascades
		int maxSuperMoveCards = (numCellsFree + 1) * (numEmptyCascades + 1);
		for (int i = 0; i < NUM_CASCADES; i++)
			if (cascade[i] != null)
				for (int j = 0; j < NUM_CASCADES; j++)
					if (j != i && cascade[j] != null) {
						Card cardTo = cascade[j].card;
						String move = cascade[j].card.toString();
						Node from = cascade[i];
						for (int numCards = 1; numCards <= maxSuperMoveCards; numCards++) {
							if (cardTo.rank == from.card.rank + 1 && cardTo.isRed() != from.card.isRed() && !prevMove.equals(move)) {
								FreeCellNode child = (FreeCellNode) childClone();
								// Create copy of numCards chain from cascade i and add it to the head of cascade j
								Node head = new Node(child.cascade[i]);
								Node current = head;
								child.cascade[i] = child.cascade[i].next;
								for (int k = 1; k < numCards; k++) {
									current.next = new Node(child.cascade[i]);
									current = current.next;
									child.cascade[i] = child.cascade[i].next;
								}
								current.next = child.cascade[j];
								child.cascade[j] = head;

								// Modify the cascade sizes accordingly
								child.cascadeSize[i] -= numCards;
								child.cascadeSize[j] += numCards;

								child.prevMove = move;
								children.add(child);
								break; // no other possible legal move if one legal move found
							}
							// Stop iterating if no next card or card isn't part of continuous, movable stack.
							if (from.next == null || from.next.card.rank != from.card.rank + 1 || from.next.card.isRed() == from.card.isRed())
								break;
						}
					}
		// Moves from cascades to empty cascade
		if (numEmptyCascades > 0) {
			maxSuperMoveCards = (numCellsFree + 1) * numEmptyCascades;
			int j = leftmostEmptyCascade;
			for (int i = 0; i < NUM_CASCADES; i++)
				if (cascade[i] != null) {
					Node from = cascade[i];
					int numCards = 1;
					Node current = from;
					for (; numCards <= maxSuperMoveCards; numCards++) {
						if (current.next == null || current.next.card.rank != current.card.rank + 1 || current.next.card.isRed() == current.card.isRed())
							break;
						current = current.next;
					}
					String move = current.card.toString();
					if (move.equals(prevMove)){
						break;
					}

					FreeCellNode child = (FreeCellNode) childClone();
					// Create copy of numCards chain from cascade i and add it to the head of cascade j
					Node head = new Node(child.cascade[i]);
					current = head;
					child.cascade[i] = child.cascade[i].next;
					for (int k = 1; k < numCards; k++) {
						current.next = new Node(child.cascade[i]);
						current = current.next;
						child.cascade[i] = child.cascade[i].next;
					}

					current.next = child.cascade[j];
					child.cascade[j] = head;

					// Modify the cascade sizes accordingly
					child.cascadeSize[i] -= numCards;
					child.cascadeSize[j] += numCards;

					child.prevMove = move;
					children.add(child);
				}
		}

		// Moves from cascades to cells
		if (numCellsFree > 0) {
			int j = 0;
			while (cell[j] != null)
				j++;
			for (int i = 0; i < NUM_CASCADES; i++)
				if (cascade[i] != null) {
					String move = cascade[i].card.toString();
					if (move.equals(prevMove)){
						break;
					}
					FreeCellNode child = (FreeCellNode) childClone();
					// Create copy of numCards chain from cascade i and add it to the head of cascade j
					child.cell[j] = child.cascade[i].card;
					child.cascade[i] = child.cascade[i].next;

					// Modify the cascade/cells-free sizes accordingly
					child.cascadeSize[i]--;
					child.numCellsFree--;

					child.prevMove = move;
					children.add(child);
				}
		}

		// Perform safe autoplay on children
		for (SearchNode child : children)
			((FreeCellNode) child).safeAutoPlay();
		return children;
	}

	@Override
	public Object clone() {
		FreeCellNode newNode = (FreeCellNode) super.clone();
		newNode.homeRank = homeRank.clone();
		newNode.cell = cell.clone();
		newNode.cascadeSize = cascadeSize.clone();
		newNode.cascade = cascade.clone();
		return newNode;
	}  

	public String toString() {
		// Home
		StringBuilder sb = new StringBuilder("Home:");
		for (int suit = 0; suit < Card.NUM_SUITS; suit++) {
			sb.append(" ");
			sb.append(homeRank[suit] == NONE ? "--" : Card.getCard(homeRank[suit], suit).toString());
		}

		// Cells
		sb.append("\nCells:");
		for (int i = 0; i < NUM_CELLS; i++) {
			sb.append(" ");
			sb.append((char) ('a' + i));
			sb.append(":");
			sb.append(cell[i] == null ? "--" : cell[i].toString());
		}
		sb.append("\n");

		// Cascades
		for (int i = 0; i < NUM_CASCADES; i++) {
			sb.append(i + 1);
			sb.append(":");
			cascadeToString(cascade[i], sb);
			sb.append("\n");
		}

		return sb.toString();
	}

	private void cascadeToString(Node node, StringBuilder sb) {
		if (node == null)
			return;
		cascadeToString(node.next, sb);
		sb.append(" ");
		sb.append(node.card);
	}

	public int boardEval() {
		int result = 0;
		for(int i = 0; i < 8; i++) {

			int homeTotal = 0;

			if (i < 4){

				//check if home ranks are filled
				int c = homeRank[i];
				if(c != -1) {
					result += (homeRank[i]+1)*2;
				}

				//check if free cells are filled
				if(cell[i] == null) {
					result++;
				}

				//check if there are any empty cascades
				if(cascade[i] == null) {
					result++;
				}

				//get the total of the top home card to compute diffFromTop later
				if (homeRank[i] != -1){
					homeTotal += homeRank[i];
				}
			}

			//

			//Calculate the number of continuous cascades
			//Below code based off of Algorithm to Solve FreeCell Solitaire Games
			Node node = cascade[i];
			while(node != null && node.next != null
					&& node.card.rank == node.next.card.rank-1
					&& node.card.isRed() != node.next.card.isRed()) {
				result++;
				node = node.next;
			}

			//Calculate the total of the lowest card in each cascade
			int cascadeTotal = 0;
			if (cascade[i] != null){
				cascadeTotal += cascade[i].card.rank;
			}

			//Calculate the average cascade total minus the average home total
			int diffFromTop  = (cascadeTotal / cascade.length) - (homeTotal / homeRank.length);
			result -= diffFromTop;
			
		}

		return result;
	}

	public int heinemanEval (int foundationIndex){
		int currRank = homeRank[foundationIndex];
		int currSuit = foundationIndex;

		int aboveCards = 0;
		for (int i = 0; i < cascade.length; i++){
			aboveCards = 0;
			if (cascade[i] != null){
				Node currNode = cascade[i];
				Card currCard = currNode.card;
				while (currCard != null && (currCard.getRank() != currRank + 1 && currCard.getSuit() != currSuit)){
					aboveCards++;
					currNode = currNode.next;
				}
			}
		}
		return aboveCards;
	}


	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	public static void main(String[] args) {
		SearchNode node = new FreeCellNode(3);
		System.out.println(node);

		for (SearchNode child : node.expand())
			System.out.println(child);

	}

}
